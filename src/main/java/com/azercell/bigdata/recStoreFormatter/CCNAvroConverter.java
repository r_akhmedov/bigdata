package com.azercell.bigdata.recStoreFormatter;

import com.azercell.bigdata.ber.BERFlatRecord;
import org.apache.avro.generic.GenericData;

import java.util.*;

/**
 * Created by rahimahmedov on 9/13/14.
 */
public class CCNAvroConverter extends BERAvroConverter {

    public static final String ccnSchemaDef =
            "{\n" +
                    "    \"type\": \"record\",\n" +
                    "    \"name\": \"ccn\",\n" +
                    "    \"fields\": [\n" +
                    "        {\"name\": \"resultCode\", \"type\": \"int\", \"default\": 0},\n" +
                    "        {\"name\": \"resultCodeExtension\", \"type\": \"int\", \"default\": 0},\n" +
                    "        {\"name\": \"triggerTime\", \"type\": [\"string\", \"null\"]},\n" +
                    "        {\"name\": \"nodeName\", \"type\": [\"string\", \"null\"]},\n" +
                    "        {\"name\": \"serviceContextID\", \"type\": [\"string\", \"null\"]},\n" +
                    "        {\"name\": \"chargingContextID\", \"type\": [\"string\", \"null\"]},\n" +
                    "        {\"name\": \"serviceSessionID\", \"type\": [\"string\", \"null\"]},\n" +
                    "        {\"name\": \"recordIdentificationNumber\", \"type\": \"int\", \"default\": 0},\n" +
                    "        {\"name\": \"partialSequenceNumber\", \"type\": \"int\", \"default\": 0},\n" +
                    "        {\"name\": \"lastPartialOutput\", \"type\": \"int\", \"default\": 0},\n" +
                    "        {\"name\": \"servedMsisdn\", \"type\": [\"string\", \"null\"]},\n" +
                    "        {\"name\": \"sessionId\", \"type\": [\"string\", \"null\"]},\n" +
                    "        {\"name\": \"originRealm\", \"type\": [\"string\", \"null\"]},\n" +
                    "        {\"name\": \"originHost\", \"type\": [\"string\", \"null\"]},\n" +
                    "        {\"name\": \"ccServiceIdentifier\", \"type\": [\"string\", \"null\"]},\n" +
                    "        {\"name\": \"ccTimeUnit\", \"type\": \"int\", \"default\": 0},\n" +
                    "        {\"name\": \"ccTotalOctetsUnit\", \"type\": \"int\", \"default\": 0},\n" +
                    "        {\"name\": \"ccServiceSpecificUnit\", \"type\": \"int\", \"default\": 0},\n" +
                    "        {\"name\": \"ccRoamingPosition\", \"type\": \"int\", \"default\": 0},\n" +
                    "        {\"name\": \"ccServiceClassID\", \"type\": \"int\", \"default\": 0},\n" +
                    "        {\"name\": \"ccAccountValueBefore\", \"type\": \"double\", \"default\": 0.0},\n" +
                    "        {\"name\": \"ccAccountValueAfter\", \"type\": \"double\", \"default\": 0.0},\n" +
                    "        {\"name\": \"ccAccumulator1ID\", \"type\": \"int\", \"default\": 0},\n" +
                    "        {\"name\": \"ccAccumulator1Before\", \"type\": \"int\", \"default\": 0},\n" +
                    "        {\"name\": \"ccAccumulator1Change\", \"type\": \"int\", \"default\": 0},\n" +
                    "        {\"name\": \"ccAccumulator1After\", \"type\": \"int\", \"default\": 0},\n" +
                    "        {\"name\": \"ccAccumulator2ID\", \"type\": \"int\", \"default\": 0},\n" +
                    "        {\"name\": \"ccAccumulator2Before\", \"type\": \"int\", \"default\": 0},\n" +
                    "        {\"name\": \"ccAccumulator2Change\", \"type\": \"int\", \"default\": 0},\n" +
                    "        {\"name\": \"ccAccumulator2After\", \"type\": \"int\", \"default\": 0},\n" +
                    "        {\"name\": \"ccAccumulator3ID\", \"type\": \"int\", \"default\": 0},\n" +
                    "        {\"name\": \"ccAccumulator3Before\", \"type\": \"int\", \"default\": 0},\n" +
                    "        {\"name\": \"ccAccumulator3Change\", \"type\": \"int\", \"default\": 0},\n" +
                    "        {\"name\": \"ccAccumulator3After\", \"type\": \"int\", \"default\": 0},\n" +
                    "        {\"name\": \"ccDedicatedAccount1ID\", \"type\": \"int\", \"default\": 0},\n" +
                    "        {\"name\": \"ccDedicatedAccount1ValueBefore\", \"type\": \"double\", \"default\": 0.0},\n" +
                    "        {\"name\": \"ccDedicatedAccount1ValueAfter\", \"type\": \"double\", \"default\": 0.0},\n" +
                    "        {\"name\": \"ccDedicatedAccount1Change\", \"type\": \"double\", \"default\": 0.0},\n" +
                    "        {\"name\": \"ccDedicatedAccount1CampaignID\", \"type\": \"int\", \"default\": 0},\n" +
                    "        {\"name\": \"ccAccount1ExpiryDateBefore\", \"type\": [\"string\", \"null\"]},\n" +
                    "        {\"name\": \"ccAccount1ExpiryDateAfter\", \"type\": [\"string\", \"null\"]},\n" +
                    "        {\"name\": \"ccDA1UnitsBefore\", \"type\": \"int\", \"default\": 0},\n" +
                    "        {\"name\": \"ccDA1UnitsAfter\", \"type\": \"int\", \"default\": 0},\n" +
                    "        {\"name\": \"ccDA1UnitsChange\", \"type\": \"int\", \"default\": 0},\n" +
                    "        {\"name\": \"ccDA1UnitType\", \"type\": \"int\", \"default\": 0},\n" +
                    "        {\"name\": \"ccDedicatedAccount2ID\", \"type\": \"int\", \"default\": 0},\n" +
                    "        {\"name\": \"ccDedicatedAccount2ValueBefore\", \"type\": \"double\", \"default\": 0.0},\n" +
                    "        {\"name\": \"ccDedicatedAccount2ValueAfter\", \"type\": \"double\", \"default\": 0.0},\n" +
                    "        {\"name\": \"ccDedicatedAccount2Change\", \"type\": \"double\", \"default\": 0.0},\n" +
                    "        {\"name\": \"ccDedicatedAccount2CampaignID\", \"type\": \"int\", \"default\": 0},\n" +
                    "        {\"name\": \"ccAccount2ExpiryDateBefore\", \"type\": [\"string\", \"null\"]},\n" +
                    "        {\"name\": \"ccAccount2ExpiryDateAfter\", \"type\": [\"string\", \"null\"]},\n" +
                    "        {\"name\": \"ccDA2UnitsBefore\", \"type\": \"int\", \"default\": 0},\n" +
                    "        {\"name\": \"ccDA2UnitsAfter\", \"type\": \"int\", \"default\": 0},\n" +
                    "        {\"name\": \"ccDA2UnitsChange\", \"type\": \"int\", \"default\": 0},\n" +
                    "        {\"name\": \"ccDA2UnitType\", \"type\": \"int\", \"default\": 0},\n" +
                    "        {\"name\": \"ccDedicatedAccount3ID\", \"type\": \"int\", \"default\": 0},\n" +
                    "        {\"name\": \"ccDedicatedAccount3ValueBefore\", \"type\": \"double\", \"default\": 0.0},\n" +
                    "        {\"name\": \"ccDedicatedAccount3ValueAfter\", \"type\": \"double\", \"default\": 0.0},\n" +
                    "        {\"name\": \"ccDedicatedAccount3Change\", \"type\": \"double\", \"default\": 0.0},\n" +
                    "        {\"name\": \"ccDedicatedAccount3CampaignID\", \"type\": \"int\", \"default\": 0},\n" +
                    "        {\"name\": \"ccAccount3ExpiryDateBefore\", \"type\": [\"string\", \"null\"]},\n" +
                    "        {\"name\": \"ccAccount3ExpiryDateAfter\", \"type\": [\"string\", \"null\"]},\n" +
                    "        {\"name\": \"ccDA3UnitsBefore\", \"type\": \"int\", \"default\": 0},\n" +
                    "        {\"name\": \"ccDA3UnitsAfter\", \"type\": \"int\", \"default\": 0},\n" +
                    "        {\"name\": \"ccDA3UnitsChange\", \"type\": \"int\", \"default\": 0},\n" +
                    "        {\"name\": \"ccDA3UnitType\", \"type\": \"int\", \"default\": 0},\n" +
                    "\n" +
                    "        {\"name\": \"ccFamilyAndFriendsID\", \"type\": \"int\", \"default\": 0},\n" +
                    "        {\"name\": \"ccFamilyAndFriendsNo\", \"type\": [\"string\", \"null\"]},\n" +
                    "        {\"name\": \"ccServiceOfferings\", \"type\": \"int\", \"default\": 0},\n" +
                    "        {\"name\": \"ccAccountValueDeducted\", \"type\": \"double\", \"default\": 0.0},\n" +
                    "        {\"name\": \"apn\", \"type\": [\"string\", \"null\"]},\n" +
                    "        {\"name\": \"chargingId\", \"type\": \"int\", \"default\": 0},\n" +
                    "        {\"name\": \"ratingGroup\", \"type\": [\"string\", \"null\"]},\n" +
                    "        {\"name\": \"cellid\", \"type\": [\"string\", \"null\"]},\n" +
                    "        {\"name\": \"locno\", \"type\": [\"string\", \"null\"]},\n" +
                    "        {\"name\": \"opnumber\", \"type\": [\"string\", \"null\"]},\n" +
                    "        {\"name\": \"exttext\", \"type\": [\"string\", \"null\"]},\n" +
                    "        {\"name\": \"mnpPrefix\", \"type\": [\"string\", \"null\"]},\n" +
                    "        {\"name\": \"mnpResult\", \"type\": \"int\", \"default\": 0},\n" +
                    "\n" +
                    "        {\"name\": \"__rawrec__\", \"type\": \"bytes\"}\n" +
                    "    ]\n" +
                    "}";

    public CCNAvroConverter(String avroSchemaDef) {
        super(avroSchemaDef);
    }

    public CCNAvroConverter() {
        this(ccnSchemaDef);
    }

    private static String formatDateTime(String datetime) {
        StringBuilder res = new StringBuilder();
        res.append("20");
        for(int i=0; i<datetime.substring(0,12).length(); i+=2)
            res.append(datetime.charAt(i+1)).append(datetime.charAt(i));
        res.append( datetime.substring(12,14).equals("30") ? "+" :"-" );
        for(int i=0; i<datetime.substring(14,18).length(); i+=2)
            res.append(datetime.charAt(14+i+1)).append(datetime.charAt(14+i));
        return res.toString();
    }

    private static String getCCParamsString(int ccid, ArrayList<BERFlatRecord> ctx) {
        for(BERFlatRecord r: ctx) {
            if ((int)r.getValueAsInteger("16.0") == ccid)
                return r.getValueAsString("16.1.7");
        }
        return null;
    }

    private static long getCCParamsInteger(int ccid, ArrayList<BERFlatRecord> ctx) {
        for(BERFlatRecord r: ctx) {
            if ((int)r.getValueAsInteger("16.0") == ccid) {
                if (r.getValueAsInteger("16.1.1") != 0)
                    return r.getValueAsInteger("16.1.1");
                if (r.getValueAsInteger("16.1.2") != 0)
                    return r.getValueAsInteger("16.1.2");
                if (r.getValueAsInteger("16.1.3") != 0)
                    return r.getValueAsInteger("16.1.3");
                if (r.getValueAsInteger("16.1.4") != 0)
                    return r.getValueAsInteger("16.1.4");
            }
        }
        return 0;
    }

    private static String getCCParamsOctet(int ccid, ArrayList<BERFlatRecord> ctx) {
        for(BERFlatRecord r: ctx) {
            if ((int)r.getValueAsInteger("16.0") == ccid)
                return r.getValueAsOctet("16.1.6");
        }
        return null;
    }

    private static String getCCParamsPartyNum(int ccid, ArrayList<BERFlatRecord> ctx) {
        for(BERFlatRecord r: ctx) {
            if ((int)r.getValueAsInteger("16.0") == ccid) {
                if (r.getValueAsOctet("16.1.9.0") != null)
                    return formatNumber(r.getValueAsOctet("16.1.9.0"));
                return null;
            }
        }
        return null;
    }

    private static String formatNumber(String str) {
        StringBuilder res = new StringBuilder();
        if (str == null) return null;
        for(int i=0; i<str.length(); i+=2)
            res.append(str.charAt(i + 1)).append(str.charAt(i));
        return res.toString();
    }

    static class Accum {
        long accumId, accumB, accumA, accumC;
        Accum(long i, long b, long a, long c) {
            accumB = b;
            accumA = a;
            accumC = c;
            accumId = i;
        }
    }

    static class DedicatedAcc {
        long daID, daUB, daUA, daUC, cmgnID, daUT;
        String expDateB, expDateA;
        double daB, daA, daC;

        DedicatedAcc(long daID, double daB, double daA, double daC,
                     long daUB, long daUA, long daUC, long cmgnID, long daUT,
                     String expDateB, String expDateA) {
            this.daID = daID;
            this.daB = daB;
            this.daA = daA;
            this.daC = daC;
            this.daUB = daUB;
            this.daUA = daUA;
            this.daUC = daUC;
            this.cmgnID = cmgnID;
            this.daUT = daUT;
            this.expDateB = expDateB;
            this.expDateA = expDateA;
        }
    }

    @Override
    public void convert(GenericData.Record avroRecord, BERFlatRecord berRecord) {
        //avroRecord.put("servedMsisdn", berRec.getValueAsString("6.10[0].1"));

        avroRecord.put("resultCode", berRecord.getValueAsInteger("6.0"));
        avroRecord.put("resultCodeExtension", berRecord.getValueAsInteger("6.1"));
        avroRecord.put("triggerTime",  formatDateTime(berRecord.getValueAsOctet("6.2")));
        avroRecord.put("nodeName", berRecord.getValueAsString("6.3"));
        avroRecord.put("serviceContextID", berRecord.getValueAsString("6.4"));
        avroRecord.put("chargingContextID", berRecord.getValueAsString("6.5"));
        avroRecord.put("serviceSessionID", berRecord.getValueAsString("6.6"));
        avroRecord.put("recordIdentificationNumber", berRecord.getValueAsInteger("6.7"));
        avroRecord.put("partialSequenceNumber", berRecord.getValueAsInteger("6.8"));
        avroRecord.put("lastPartialOutput", berRecord.getValueAsInteger("6.9"));
        for (BERFlatRecord rc: berRecord.getChildsArrayList("6.10.16")) {
            if (rc.getValueAsInteger("16.0") == 0) {
                avroRecord.put("servedMsisdn", rc.getValueAsString("16.1"));
                break;
            }
        }
        if (berRecord.getValueAsString("6.11.3.2") != null)
            avroRecord.put("sessionId", berRecord.getValueAsString("6.11.3.2"));
        if (berRecord.getValueAsString("6.12.0.0") != null)
            avroRecord.put("originRealm", berRecord.getValueAsString("6.12.0.0"));
        if (berRecord.getValueAsString("6.12.0.1") != null)
            avroRecord.put("originHost", berRecord.getValueAsString("6.12.0.1"));

        HashSet<Long> sids = new HashSet<Long>();
        HashSet<Long> rids = new HashSet<Long>();
        long timeunit=0, servspecific=0, octetunit=0;
        double accBefore = -99999999.999999f, accAfter = 99999999.999999f, charge=0.0f;
        SortedSet<Map.Entry<Long, Accum>> accums = new TreeSet<Map.Entry<Long, Accum>>(
                new Comparator<Map.Entry<Long, Accum>>() {
                    @Override
                    public int compare(Map.Entry<Long, Accum> o1, Map.Entry<Long, Accum> o2) {
                        if (o2.getValue().accumC > o1.getValue().accumC) return 1;
                        if (o2.getValue().accumC < o1.getValue().accumC) return -1;
                        return 0;
                        //return (o2.getValue().accumC-o1.getValue().accumC);
                    }
                });
        TreeMap<Long, Accum> accumsTM = new TreeMap<Long, Accum>();

        SortedSet<Map.Entry<Long, DedicatedAcc>> dedaccs = new TreeSet<Map.Entry<Long, DedicatedAcc>>(
                new Comparator<Map.Entry<Long, DedicatedAcc>>() {
                    @Override
                    public int compare(Map.Entry<Long, DedicatedAcc> o1, Map.Entry<Long, DedicatedAcc> o2) {
                        if (o2.getValue().daC != 0) {
                            if (o2.getValue().daC > o1.getValue().daC) return 1;
                            if (o2.getValue().daC < o1.getValue().daC) return -1;
                        }
                        else {
                            if (o2.getValue().daUC > o1.getValue().daUC) return 1;
                            if (o2.getValue().daUC < o1.getValue().daUC) return -1;
                        }
                        return 0;
                        //return (o2.getValue().accumC-o1.getValue().accumC);
                    }
                });
        TreeMap<Long, DedicatedAcc> dedaccsTM = new TreeMap<Long, DedicatedAcc>();

        for (BERFlatRecord rc: berRecord.getChildsArrayList("6.13.0")) {
            sids.add(Long.valueOf(rc.getValueAsInteger("0.1")));
            if (getCCParamsInteger(16778254, rc.getChildsArrayList("0.10.16")) != 0)
                rids.add(getCCParamsInteger(16778254, rc.getChildsArrayList("0.10.16")));

            timeunit += rc.getValueAsInteger("0.2.16[0].1");
            octetunit += rc.getValueAsInteger("0.2.16[0].3");
            servspecific += rc.getValueAsInteger("0.2.16[0].6");

            // 0.9.3.0 -amt 0.9.3.1 -dec
            accBefore = rc.getValueAsInteger("0.9.3.0")/Math.pow(10,rc.getValueAsInteger("0.9.3.1")) > accBefore ? rc.getValueAsInteger("0.9.3.0")/Math.pow(10, rc.getValueAsInteger("0.9.3.1")) : accBefore;
            accAfter = rc.getValueAsInteger("0.9.4.0")/Math.pow(10, rc.getValueAsInteger("0.9.4.1")) < accAfter ? rc.getValueAsInteger("0.9.4.0")/Math.pow(10, rc.getValueAsInteger("0.9.4.1")) : accAfter;

            for (BERFlatRecord ac: rc.getChildsArrayList("0.9.6.16")) {
                if (accumsTM.containsKey(ac.getValueAsInteger("16.0"))) {
                    Accum acc = accumsTM.get(ac.getValueAsInteger("16.0"));
                    accumsTM.put(ac.getValueAsInteger("16.0"),
                            new Accum(ac.getValueAsInteger("16.0"),
                                    acc.accumB < ac.getValueAsInteger("16.1") ? ac.getValueAsInteger("16.1") : acc.accumB,
                                    acc.accumA > ac.getValueAsInteger("16.3") ? ac.getValueAsInteger("16.3") : acc.accumA,
                                    acc.accumC + ac.getValueAsInteger("16.2")));
                }
                else
                    accumsTM.put(ac.getValueAsInteger("16.0"),
                            new Accum(ac.getValueAsInteger("16.0"), ac.getValueAsInteger("16.1"), ac.getValueAsInteger("16.3"), ac.getValueAsInteger("16.2")));
            }
            accums.addAll(accumsTM.entrySet());
            for (BERFlatRecord dc: rc.getChildsArrayList("0.9.7.16")) {
                if(dedaccsTM.containsKey(dc.getValueAsInteger("16.0"))) {
                    DedicatedAcc dea = dedaccsTM.get(dc.getValueAsInteger("16.0"));
                    dedaccsTM.put(dc.getValueAsInteger("16.0"),
                            new DedicatedAcc(
                                    dc.getValueAsInteger("16.0"),
                                    dea.daB < dc.getValueAsInteger("16.1.0")/Math.pow(10, dc.getValueAsInteger("16.1.1")) ? dc.getValueAsInteger("16.1.0")/Math.pow(10, dc.getValueAsInteger("16.1.1")) : dea.daB,
                                    dea.daA > dc.getValueAsInteger("16.2.0")/Math.pow(10, dc.getValueAsInteger("16.2.1")) ? dc.getValueAsInteger("16.2.0")/Math.pow(10, dc.getValueAsInteger("16.2.1")) : dea.daA,
                                    dea.daC += dc.getValueAsInteger("16.3.0")/Math.pow(10, dc.getValueAsInteger("16.3.1")),
                                    dea.daUB < dc.getValueAsInteger("16.11") ? dc.getValueAsInteger("16.11") : dea.daUB,
                                    dea.daUA < dc.getValueAsInteger("16.12") ? dc.getValueAsInteger("16.12") : dea.daUA,
                                    dea.daUC += dc.getValueAsInteger("16.13"),
                                    dc.getValueAsInteger("16.4"),
                                    dc.getValueAsInteger("16.14"),
                                    dc.getValueAsString("16.5") != null && dea.expDateB.compareTo(dc.getValueAsString("16.5")) > 0 ? dc.getValueAsString("16.5") : dea.expDateB,
                                    dc.getValueAsString("16.6") != null && dea.expDateA.compareTo(dc.getValueAsString("16.6")) < 0 ? dc.getValueAsString("16.6") : dea.expDateA ) );
                }
                else
                    dedaccsTM.put(dc.getValueAsInteger("16.0"),
                            new DedicatedAcc(
                                    dc.getValueAsInteger("16.0"),
                                    dc.getValueAsInteger("16.1.0")/Math.pow(10, dc.getValueAsInteger("16.1.1")),
                                    dc.getValueAsInteger("16.2.0")/Math.pow(10, dc.getValueAsInteger("16.2.1")),
                                    dc.getValueAsInteger("16.3.0")/Math.pow(10, dc.getValueAsInteger("16.3.1")),
                                    dc.getValueAsInteger("16.11"),
                                    dc.getValueAsInteger("16.12"),
                                    dc.getValueAsInteger("16.13"),
                                    dc.getValueAsInteger("16.4"),
                                    dc.getValueAsInteger("16.14"),
                                    dc.getValueAsString("16.5"),
                                    dc.getValueAsString("16.6")) );
            }

            charge += rc.getValueAsInteger("0.9.13.0")/Math.pow(10, rc.getValueAsInteger("0.9.13.1"));
        }
        dedaccs.addAll(dedaccsTM.entrySet());

        StringBuilder sids_str = new StringBuilder();
        for (Object val: sids.toArray())
            sids_str.append((Long)val).append(",");
        if (sids_str.toString() != null)
            avroRecord.put("ccServiceIdentifier",  sids_str.toString().substring(0, sids_str.length()-1 < 0 ? 0 : sids_str.length()-1) );
        avroRecord.put("ccTimeUnit", timeunit);
        avroRecord.put("ccTotalOctetsUnit", octetunit);
        avroRecord.put("ccServiceSpecificUnit", servspecific);
        avroRecord.put("ccRoamingPosition", berRecord.getValueAsInteger("6.13.0.7"));
        avroRecord.put("ccServiceClassID", berRecord.getValueAsInteger("6.13.0.9.1"));
        avroRecord.put("ccAccountValueBefore", accBefore);
        avroRecord.put("ccAccountValueAfter", accAfter);
        int i=0;
        Iterator<Map.Entry<Long, Accum>> acci = accums.iterator();
        //while (acci.hasNext() && i < 3 ) {
        while (i < 3 ) {
            //Accum acc = acci.next().getValue();
            Accum acc = acci.hasNext() ? acci.next().getValue() : new Accum(0,0,0,0);
            avroRecord.put("ccAccumulator"+(i+1)+"ID", acc.accumId);
            avroRecord.put("ccAccumulator"+(i+1)+"Before", acc.accumB);
            avroRecord.put("ccAccumulator"+(i+1)+"Change", acc.accumC);
            avroRecord.put("ccAccumulator"+(i+1)+"After", acc.accumA);
            i++;
        }
        i=0;
        Iterator<Map.Entry<Long, DedicatedAcc>> deai = dedaccs.iterator();
        //while (deai.hasNext() && i < 3 ) {
        while (i<3) {
            //DedicatedAcc dea = deai.next().getValue();
            DedicatedAcc dea = deai.hasNext() ? deai.next().getValue() : new DedicatedAcc(0, 0, 0, 0, 0, 0, 0, 0, 0, null, null);
            avroRecord.put("ccDedicatedAccount"+(i+1)+"ID", dea.daID);
            avroRecord.put("ccDedicatedAccount"+(i+1)+"ValueBefore", dea.daB);
            avroRecord.put("ccDedicatedAccount"+(i+1)+"ValueAfter", dea.daA);
            avroRecord.put("ccDedicatedAccount"+(i+1)+"Change", dea.daC);
            avroRecord.put("ccDedicatedAccount"+(i+1)+"CampaignID", dea.cmgnID);
            if (dea.expDateB != null)
                avroRecord.put("ccAccount"+(i+1)+"ExpiryDateBefore", dea.expDateB);
            if (dea.expDateA != null)
                avroRecord.put("ccAccount"+(i+1)+"ExpiryDateAfter", dea.expDateA);
            avroRecord.put("ccDA"+(i+1)+"UnitsBefore", dea.daUB);
            avroRecord.put("ccDA"+(i+1)+"UnitsAfter", dea.daUA);
            avroRecord.put("ccDA"+(i+1)+"UnitsChange", dea.daUC);
            avroRecord.put("ccDA"+(i+1)+"UnitType", dea.daUT);
            i++;
        }

        avroRecord.put("ccFamilyAndFriendsID", berRecord.getValueAsInteger("6.13.0.9.8"));
        if (berRecord.getValueAsOctet("6.13.0.9.9") != null)
            avroRecord.put("ccFamilyAndFriendsNo", formatNumber(berRecord.getValueAsOctet("6.13.0.9.9")));
        avroRecord.put("ccServiceOfferings", berRecord.getValueAsInteger("6.13.0.9.10"));
        avroRecord.put("ccAccountValueDeducted", charge);
        String apn = getCCParamsString(16778235, berRecord.getChildsArrayList("6.13.0.10.16"));
        if (apn != null)
            avroRecord.put("apn", apn);
        avroRecord.put("chargingId", getCCParamsInteger(16778226, berRecord.getChildsArrayList("6.13.0.10.16")));
        sids_str = new StringBuilder();
        for (Object val: rids.toArray())
            sids_str.append((Long)val).append(",");
        if (sids_str.toString() != null)
            avroRecord.put("ratingGroup",  sids_str.toString().substring(0, sids_str.length()-1 < 0 ? 0 : sids_str.length()-1) );
        String cellid = getCCParamsOctet(16778250, berRecord.getChildsArrayList("6.13.0.10.16")) +
                (getCCParamsOctet(16778251, berRecord.getChildsArrayList("6.13.0.10.16")) != null ? getCCParamsOctet(16778251, berRecord.getChildsArrayList("6.13.0.10.16")) : getCCParamsOctet(16778252, berRecord.getChildsArrayList("6.13.0.10.16")));
        if (cellid != null)
            avroRecord.put("cellid", cellid );
        String locno = getCCParamsString(16778217, berRecord.getChildsArrayList("6.13.0.10.16"));
        if (locno != null)
            avroRecord.put("locno", locno);
        String opnumber = getCCParamsPartyNum(16778219, berRecord.getChildsArrayList("6.13.0.10.16"));
        if (opnumber != null)
            avroRecord.put("opnumber", opnumber);
        String exttext = getCCParamsString(16777417, berRecord.getChildsArrayList("6.13.0.10.16"));
        if (exttext != null)
            avroRecord.put("exttext", exttext);
        String mnpPrefix = getCCParamsString(16778220, berRecord.getChildsArrayList("6.13.0.10.16"));
        if (mnpPrefix != null)
            avroRecord.put("mnpPrefix", mnpPrefix);
        avroRecord.put("mnpResult", getCCParamsInteger(16778222, berRecord.getChildsArrayList("6.13.0.10.16")));

    }



}
