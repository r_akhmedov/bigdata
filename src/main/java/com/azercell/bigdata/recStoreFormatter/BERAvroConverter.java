/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.azercell.bigdata.recStoreFormatter;


import com.azercell.bigdata.ber.BERFlatRecord;
import org.apache.avro.Schema;
import org.apache.avro.generic.GenericData;
import org.apache.avro.generic.GenericRecord;

import java.nio.*;

/**
 *
 * @author itrahim
 */
public abstract class BERAvroConverter {

    private final GenericData.Record avroRecord;


    public BERAvroConverter(String avroSchemaDef) {
        Schema.Parser parser = new Schema.Parser();
        Schema schema = parser.parse(avroSchemaDef);
        this.avroRecord = new GenericData.Record(schema);
    } 
    
    public abstract void convert(GenericData.Record avroRecord, BERFlatRecord berRec);

    public GenericRecord convertAndGet(BERFlatRecord berRec) {
        this.convert(this.avroRecord, berRec);
        byte[] rawdata  = berRec.getByteArray();
        this.avroRecord.put("__rawrec__", ByteBuffer.wrap(rawdata));
        return this.avroRecord;
    }

    public String getSchemaString() {
        return this.avroRecord.getSchema().toString(true);
    }

    public GenericData.Record  getRecord() {
        return this.avroRecord;
    }

    public Schema getSchema() {
        return this.avroRecord.getSchema();
    }
    
}
