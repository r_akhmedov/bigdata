package com.azercell.bigdata;

import com.azercell.bigdata.ber.BERFlatDecoder;
import com.azercell.bigdata.ber.BERFlatRecord;
import com.azercell.bigdata.recStoreFormatter.CCNAvroConverter;
import org.apache.avro.file.DataFileReader;
import org.apache.avro.file.DataFileWriter;
import org.apache.avro.generic.GenericData;
import org.apache.avro.generic.GenericDatumReader;
import org.apache.avro.generic.GenericDatumWriter;
import org.apache.avro.generic.GenericRecord;

import javax.xml.crypto.Data;
import java.io.File;

import java.io.IOException;
import java.nio.ByteBuffer;

/**
 * Created by rahimahmedov on 9/13/14.
 */
public class MainClass {

    public static void main(String [] argv) throws IOException{

        CCNAvroConverter c2a = new CCNAvroConverter();

        System.out.println(c2a.getSchemaString());

        BERFlatDecoder berDecoder = new BERFlatDecoder(8192, (short)0, "/Users/rahimahmedov/Documents/workspace/bigdata/CCN5-CCNCDR44-03-Blk8192Blk-7765-20140819190323", 64*1024);

        // Write to File
        BERFlatRecord berRecord;
        DataFileWriter<GenericRecord> dataFileWriter = new DataFileWriter<GenericRecord>(new GenericDatumWriter<GenericRecord>(c2a.getSchema()));
        dataFileWriter.create(c2a.getSchema(), new File("/Users/rahimahmedov/Documents/workspace/bigdata/CCNData.avro"));
        while ((berRecord = berDecoder.decodeAndGet()) != null ) {
            GenericData.Record rec = (GenericData.Record)c2a.convertAndGet(berRecord);
            dataFileWriter.append(rec);
        }
        dataFileWriter.close();

        // Read from file
        DataFileReader<GenericRecord> fileReader =
                new DataFileReader<GenericRecord>(
                        new File("/Users/rahimahmedov/Documents/workspace/bigdata/CCNData.avro"),
                        new GenericDatumReader<GenericRecord>()
                );
        while (fileReader.hasNext()) {
            GenericRecord record = fileReader.next();
            System.out.println(record.get("servedMsisdn"));
            BERFlatDecoder dec = new BERFlatDecoder ( ((ByteBuffer)record.get("__rawrec__")).array() );
            //dec.decodeAndGet().printRawData(16);
            dec.decodeAndGet().printRecord();
            System.out.println();
        }


    }


}
