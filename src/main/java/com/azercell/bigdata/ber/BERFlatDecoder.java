package com.azercell.bigdata.ber;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.HashMap;

/**
 * Created by rahimahmedov on 9/7/14.
 */
public class BERFlatDecoder {

    protected short[] byteBuffer;
    protected BERFlatRecord berRecord;
    private int pos = 0;
    private int blkSize;
    private short blkFiller;
    private HashMap<String, int[]> tagAddrs; // = new HashMap<String, int[]>();

    private RandomAccessFile berFile;
    private int fileBlkSize;

    
    BERFlatDecoder() {
        this(0, (short)0x00);
    }

    public BERFlatDecoder(byte[] rawrec) {
        this(0, (short)0x00);
        this.byteBuffer = new short[rawrec.length];
        for(int i=0; i<rawrec.length; i++)
            this.byteBuffer[i] = (short)(rawrec[i] & 0xff);
    }

    BERFlatDecoder(int blkSize, short filler){
        this.blkSize = blkSize;
        this.blkFiller = filler;
    }

    public BERFlatDecoder(int blkSize, short blkFiller, String berFilePath, int fileBlkSize) 
        throws IOException 
    {
        this.blkSize = blkSize;
        this.blkFiller = blkFiller;
        this.fileBlkSize = fileBlkSize;
        this.berFile = new RandomAccessFile(berFilePath, "r");
        byte[] b = new byte[fileBlkSize*2];
        this.byteBuffer = new short[ fileBlkSize*2 < this.berFile.length() ? fileBlkSize*2 : (int)this.berFile.length() ]; 
        this.berFile.read(b, 0, fileBlkSize*2);
        for (int i=0; i<this.byteBuffer.length; i++)
            this.byteBuffer[i] = (short)(b[i] & 0xff);
    }
    
    public BERFlatDecoder(int blkSize, short blkFiller, String berFilePath) 
        throws IOException 
    {
        this.blkSize = blkSize;
        this.blkFiller = blkFiller;
        RandomAccessFile bf = new RandomAccessFile(berFilePath, "r");
        byte[] b = new byte[(int)bf.length()];
        this.byteBuffer = new short[(int)bf.length()]; 
        bf.readFully(b);
        for (int i=0; i<this.byteBuffer.length; i++)
            this.byteBuffer[i] = (short)(b[i] & 0xff);
    }
    
    private void readNextFileBlok() throws IOException {
        if (this.pos >= this.fileBlkSize && this.berFile.getFilePointer() < this.berFile.length()) {
            
            short[] tmpByteBuffer = this.byteBuffer.clone();
            byte [] tmpByteBuffer2 = new byte[this.fileBlkSize];
            int rc = this.berFile.read(tmpByteBuffer2, 0, this.fileBlkSize);
            rc = (rc == -1 ? 0 : rc);
            this.byteBuffer = new short[this.fileBlkSize + rc];
            for(int i=0; i<this.fileBlkSize; i++)
                this.byteBuffer[i] = tmpByteBuffer[this.fileBlkSize+i];
            for(int i=0; i<rc; i++)
                this.byteBuffer[this.fileBlkSize+i] = (short) (tmpByteBuffer2[i] & 0xff);
            this.pos -= this.fileBlkSize;
        }
    }

        
    private void addTagAddrs(String tagAddr, int tagClass){
        /* val[0] - tag count
           val[1] - tag class*/
        if(this.tagAddrs.containsKey(tagAddr))
            this.tagAddrs.put(tagAddr, new int[]{this.tagAddrs.get(tagAddr)[0]+1, tagClass});
        else
            this.tagAddrs.put(tagAddr, new int[]{0, tagClass});
    }


    private int incrPos() {
        this.berRecord.addByte((byte)this.byteBuffer[this.pos++]);
        return this.pos;
    }

    // use only for primitive !!!
    private String getTagAddr(String tagAddr) {

        StringBuilder stb  = new StringBuilder(tagAddr);
        String[] tagIDs = tagAddr.split("\\.");
        int [] val = this.tagAddrs.get(tagAddr);
        if ( (val[1] == 0 && (tagIDs[tagIDs.length-1].equals("16") || tagIDs[tagIDs.length-1].equals("17")) ) || val[0] > 0 )
            stb.append("[").append(val[0]).append("]");

        return stb.toString();
    }
    
    public void decode(String parentTagAddr) {

        // decode tag
        boolean isComposite = (this.byteBuffer[this.pos] & 0x20) == 0x20;
        int tagID =0;
        int tagClass = this.byteBuffer[this.pos] & 0xC0;
        if ( (this.byteBuffer[this.pos] & 0x1F) < 0x1F)
            tagID = this.byteBuffer[this.pos] & 0x1F;
        else {
            do {
                this.incrPos(); 
                tagID += (this.byteBuffer[this.pos] & 0x7F);
            } while ( (this.byteBuffer[this.pos] & 0x80) == 0x80);
        }
        StringBuilder strb = new StringBuilder();
        if (parentTagAddr.equals("")) {
            strb.append(tagID);
        }
        else strb.append(this.getTagAddr(parentTagAddr)).append(".").append(tagID);
        this.addTagAddrs(strb.toString(), tagClass);

        // decode length
        int length = 0;
        this.incrPos(); 
        if (this.byteBuffer[this.pos] < 0x80)
            length = this.byteBuffer[this.pos];
        else if (this.byteBuffer[this.pos] > 0x80) {
            int lenlen = this.byteBuffer[this.pos] & 0x7F;
            this.incrPos();
            length += this.byteBuffer[this.pos];
            for (int i=1; i<lenlen; i++ ) {
                this.incrPos();
                length = (length << 8) + this.byteBuffer[this.pos];
            }
        }
        else length = -1;

        // decode value
        this.incrPos(); 
        if (isComposite) {
            if(length != -1) {
                int endPos = this.pos + length;
                while(this.pos < endPos)
                    decode(strb.toString());
            }
            else {
                while(this.byteBuffer[this.pos] != 0x00 || this.byteBuffer[this.pos+1] != 0x00)
                    decode(strb.toString());
                this.incrPos();
                this.incrPos();
            }
        }
        else {
            if (length != -1) {
                short [] val = new short[length];
                int endPos = this.pos+length;
                int i=0;
                while(this.pos < endPos) 
                    val[i++] = this.byteBuffer[this.pos++];
                this.berRecord.addField(this.getTagAddr(strb.toString()), val);
            }
            else {
                int endPos = 0;
                int i =0;
                while (this.byteBuffer[this.pos] != 0x00 || this.byteBuffer[this.pos+1] != 0x00)
                    endPos ++;
                short [] val = new short[endPos+1];
                while (this.byteBuffer[this.pos] != 0x00 || this.byteBuffer[this.pos+1] != 0x00) 
                    val[i++] = this.byteBuffer[this.pos++];
                this.berRecord.addField(this.getTagAddr(strb.toString()), val);
                this.incrPos();
                this.incrPos();
            }
        }
    }

    public BERFlatRecord decodeAndGet(int posInFile) {
        this.tagAddrs = new HashMap<String, int[]>();
        this.berRecord = new BERFlatRecord();    
        this.pos = posInFile;
        this.decode("");
        return this.berRecord;
    }
    
    public BERFlatRecord decodeAndGet() throws IOException {
        
        if(this.berFile != null)
            readNextFileBlok();
        
        if (this.blkSize > 0) {
            if (this.pos < this.byteBuffer.length && this.byteBuffer[this.pos] == this.blkFiller) {
                this.pos += this.blkSize - this.pos % this.blkSize;
            }
        }
        
        if (this.pos < this.byteBuffer.length)
            return this.decodeAndGet(this.pos);
        else return null;

    }
    
    public int getCurrentPos() {
        return this.pos;
    }

    public long getCurrentFilePos() throws IOException {
        return this.berFile.getFilePointer();
    }

}
