/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.azercell.bigdata.ber;

import java.util.ArrayList;

/**
 *
 * @author itrahim
 */
public class BERDecoder {
    
    //protected BERField resultField;
    protected byte[] inputStream;
    protected int currentPos;
    
    protected BERField decode(int starPos) {

        BERField resultField;
        this.currentPos = starPos;

        // 1. decode tag
        if ( (this.inputStream[this.currentPos] & 0x20) == 0x20) {
            resultField = new BERFieldComposite();
        }
        else {
            resultField = new BERFieldPrimitive();
        }
        resultField.tagClass = (short)(this.inputStream[this.currentPos] & 0xC0);
        if ( (this.inputStream[this.currentPos] & 0x1F) < 0x1F)
            resultField.tagID = this.inputStream[this.currentPos] & 0x1F;
        else {
            do {
                this.currentPos++;
                resultField.tagID += (this.inputStream[this.currentPos] & 0x7F);
            } while ( (this.inputStream[this.currentPos] & 0x80) == 0x80);
        }

        // 2. decode length
        this.currentPos++;
        if (this.inputStream[this.currentPos] < 0x80)
            resultField.length = this.inputStream[this.currentPos];
        else if (this.inputStream[this.currentPos] > 0x80) {
            this.currentPos++;
            resultField.length += this.inputStream[this.currentPos];
            int lenlen = this.inputStream[this.currentPos] &  0x7F;
            for (int i=1; i<lenlen; i++ ) {
                this.currentPos++;
                resultField.length = (resultField.length << 8) + this.inputStream[this.currentPos];
            }
        }
        else resultField.length = -1;

        /*   3. decode value
                while end-of-record
                    decode()
                end
           return BERFieldObject */
        this.currentPos++;
        if (resultField instanceof  BERFieldComposite) {
            ((BERFieldComposite) resultField).childFields = new ArrayList<BERField>();
            if (resultField.length != -1) {
                int endPos = this.currentPos + resultField.length;
                while (this.currentPos < endPos) {
                    ((BERFieldComposite) resultField).childFields.add(this.decode(this.currentPos));
                }
                this.currentPos++;
            }
            else {
                while (this.inputStream[this.currentPos+1] != 0x00 || this.inputStream[this.currentPos+2] != 0x00) {
                    ((BERFieldComposite) resultField).childFields.add(this.decode(this.currentPos));
                }
                this.currentPos += 2;
            }
        }
        else {
            if (resultField.length != -1) {
                resultField.valueHex = new byte[resultField.length];
                for (int i=0; i<resultField.length; i++)
                        resultField.valueHex[i] = this.inputStream[this.currentPos++];
                this.currentPos++;
            }
            else {
                int len=0;
                int beginPos=this.currentPos;
                while (this.inputStream[beginPos+1] != 0x00 || this.inputStream[beginPos+2] != 0x00) {
                    len++; beginPos++;
                }
                resultField.valueHex = new byte[len++];
                for (int i=0; i<len; i++)
                    resultField.valueHex[i] = this.inputStream[this.currentPos++];
                this.currentPos += 2;
            }
        }

        return resultField;

    }


}
