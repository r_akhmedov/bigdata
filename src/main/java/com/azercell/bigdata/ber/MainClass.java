package com.azercell.bigdata.ber;

import com.azercell.bigdata.recStoreFormatter.CCNAvroConverter;

import java.io.IOException;
import java.util.*;

/**
 * Created by rahimahmedov on 9/8/14.
 */
public class MainClass {

    private static String formatDateTime(String datetime) {
        StringBuilder res = new StringBuilder();
        res.append("20");
        for(int i=0; i<datetime.substring(0,12).length(); i+=2)
            res.append(datetime.charAt(i+1)).append(datetime.charAt(i));
        res.append( datetime.substring(12,14).equals("30") ? "+" :"-" );
        for(int i=0; i<datetime.substring(14,18).length(); i+=2)
            res.append(datetime.charAt(14+i+1)).append(datetime.charAt(14+i));
        return res.toString();
    }

    private static String getCCParamsString(int ccid, ArrayList<BERFlatRecord> ctx) {
        for(BERFlatRecord r: ctx) {
            if ((int)r.getValueAsInteger("16.0") == ccid)
                return r.getValueAsString("16.1.7");
        }
        return null;
    }

    private static long getCCParamsInteger(int ccid, ArrayList<BERFlatRecord> ctx) {
        for(BERFlatRecord r: ctx) {
            if ((int)r.getValueAsInteger("16.0") == ccid) {
                if (r.getValueAsInteger("16.1.1") != 0)
                    return r.getValueAsInteger("16.1.1");
                if (r.getValueAsInteger("16.1.2") != 0)
                    return r.getValueAsInteger("16.1.2");
                if (r.getValueAsInteger("16.1.3") != 0)
                    return r.getValueAsInteger("16.1.3");
                if (r.getValueAsInteger("16.1.4") != 0)
                    return r.getValueAsInteger("16.1.4");
            }
        }
        return 0;
    }

    private static String getCCParamsOctet(int ccid, ArrayList<BERFlatRecord> ctx) {
        for(BERFlatRecord r: ctx) {
            if ((int)r.getValueAsInteger("16.0") == ccid)
                return r.getValueAsOctet("16.1.6");
        }
        return null;
    }

    private static String getCCParamsPartyNum(int ccid, ArrayList<BERFlatRecord> ctx) {
        for(BERFlatRecord r: ctx) {
            if ((int)r.getValueAsInteger("16.0") == ccid) {
                if (r.getValueAsOctet("16.1.9.0") != null)
                    return formatNumber(r.getValueAsOctet("16.1.9.0"));
                return null;
            }
        }
        return null;
    }

    private static String formatNumber(String str) {
        StringBuilder res = new StringBuilder();
        if (str == null) return null;
        for(int i=0; i<str.length(); i+=2)
            res.append(str.charAt(i + 1)).append(str.charAt(i));
        return res.toString();
    }

    static class Accum {
        long accumId, accumB, accumA, accumC;
        Accum(long i, long b, long a, long c) {
            accumB = b;
            accumA = a;
            accumC = c;
            accumId = i;
        }
    }

    static class DedicatedAcc {
        long daID, daUB, daUA, daUC, cmgnID, daUT;
        String expDateB, expDateA;
        double daB, daA, daC;
        
        DedicatedAcc(long daID, double daB, double daA, double daC, 
                     long daUB, long daUA, long daUC, long cmgnID, long daUT, 
                     String expDateB, String expDateA) {
            this.daID = daID;
            this.daB = daB;
            this.daA = daA;
            this.daC = daC;
            this.daUB = daUB;
            this.daUA = daUA;
            this.daUC = daUC;
            this.cmgnID = cmgnID;
            this.daUT = daUT;
            this.expDateB = expDateB;
            this.expDateA = expDateA;
        }
    }
    
    public static void  main(String[] argv) throws IOException {


        BERFlatDecoder berDecoder =  new BERFlatDecoder(8*1024, (short)0, "/Users/rahimahmedov/Documents/workspace/bigdata/CCN6-CCNCDR44-06-Blk8192Blk-9999-20140923125127", 64*1024);
        //BERFlatDecoder berDecoder =  new BERFlatDecoder(8*1024, (short)0, "C:\\Users\\itrahim\\NetBeansProjects\\RecStoreFormatter\\CCN5-CCNCDR44-03-Blk8192Blk-7765-20140819190323", 64*1024);
        BERFlatRecord berRecord;
        //berRecord = berDecoder.decodeAndGet();
        while ( (berRecord = berDecoder.decodeAndGet()) != null) {
            //berRecord.printRecord();
            //berRecord.printRawData(16);
            //System.out.println();

            System.out.println("resultCode => "+berRecord.getValueAsInteger("6.0"));
            System.out.println("resultCodeExtension => "+berRecord.getValueAsInteger("6.1"));
            System.out.println("triggerTime => "+ formatDateTime(berRecord.getValueAsOctet("6.2")));
            System.out.println("nodeName => "+berRecord.getValueAsString("6.3"));
            System.out.println("serviceContextID => "+berRecord.getValueAsString("6.4"));
            System.out.println("chargingContextID => "+berRecord.getValueAsString("6.5"));
            System.out.println("serviceSessionID => "+berRecord.getValueAsString("6.6"));
            System.out.println("recordIdentificationNumber => "+berRecord.getValueAsInteger("6.7"));
            System.out.println("partialSequenceNumber => "+berRecord.getValueAsInteger("6.8"));
            System.out.println("lastPartialOutput => "+berRecord.getValueAsInteger("6.9"));
            System.out.print("servedMsisdn => ");
                for (BERFlatRecord rc: berRecord.getChildsArrayList("6.10.16")) {
                    if (rc.getValueAsInteger("16.0") == 0) {
                        System.out.println(rc.getValueAsString("16.1"));
                        break;
                    }
                }
            System.out.println("sessionId => "+berRecord.getValueAsString("6.11.3.2"));
            System.out.println("originRealm => "+berRecord.getValueAsString("6.12.0.0"));
            System.out.println("originHost => "+berRecord.getValueAsString("6.12.0.1"));

            HashSet<Long> sids = new HashSet<Long>();
            HashSet<Long> rids = new HashSet<Long>();
            long timeunit=0, servspecific=0, octetunit=0;
            double accBefore = -99999999.999999f, accAfter = 99999999.999999f, charge=0.0f;
            SortedSet<Map.Entry<Long, Accum>> accums = new TreeSet<Map.Entry<Long, Accum>>(
                    new Comparator<Map.Entry<Long, Accum>>() {
                        @Override
                        public int compare(Map.Entry<Long, Accum> o1, Map.Entry<Long, Accum> o2) {
                            if (o2.getValue().accumC > o1.getValue().accumC) return 1;
                            if (o2.getValue().accumC < o1.getValue().accumC) return -1;
                            return 0;
                            //return (o2.getValue().accumC-o1.getValue().accumC);
                        }
                    });
            TreeMap<Long, Accum> accumsTM = new TreeMap<Long, Accum>();
            
            SortedSet<Map.Entry<Long, DedicatedAcc>> dedaccs = new TreeSet<Map.Entry<Long, DedicatedAcc>>(
                    new Comparator<Map.Entry<Long, DedicatedAcc>>() {
                        @Override
                        public int compare(Map.Entry<Long, DedicatedAcc> o1, Map.Entry<Long, DedicatedAcc> o2) {
                            if (o2.getValue().daC != 0) {
                                if (o2.getValue().daC > o1.getValue().daC) return 1;
                                if (o2.getValue().daC < o1.getValue().daC) return -1;
                            }
                            else {
                                if (o2.getValue().daUC > o1.getValue().daUC) return 1;
                                if (o2.getValue().daUC < o1.getValue().daUC) return -1;
                            }    
                            return 0;
                            //return (o2.getValue().accumC-o1.getValue().accumC);
                        }
                    });
            TreeMap<Long, DedicatedAcc> dedaccsTM = new TreeMap<Long, DedicatedAcc>();
            
            for (BERFlatRecord rc: berRecord.getChildsArrayList("6.13.0")) {
                sids.add(Long.valueOf(rc.getValueAsInteger("0.1")));
                if (getCCParamsInteger(16778254, rc.getChildsArrayList("0.10.16")) != 0)
                    rids.add(getCCParamsInteger(16778254, rc.getChildsArrayList("0.10.16")));

                timeunit += rc.getValueAsInteger("0.2.16[0].1");
                octetunit += rc.getValueAsInteger("0.2.16[0].3");
                servspecific += rc.getValueAsInteger("0.2.16[0].6");

                // 0.9.3.0 -amt 0.9.3.1 -dec
                accBefore = rc.getValueAsInteger("0.9.3.0")/Math.pow(10,rc.getValueAsInteger("0.9.3.1")) > accBefore ? rc.getValueAsInteger("0.9.3.0")/Math.pow(10, rc.getValueAsInteger("0.9.3.1")) : accBefore;
                accAfter = rc.getValueAsInteger("0.9.4.0")/Math.pow(10, rc.getValueAsInteger("0.9.4.1")) < accAfter ? rc.getValueAsInteger("0.9.4.0")/Math.pow(10, rc.getValueAsInteger("0.9.4.1")) : accAfter;

                for (BERFlatRecord ac: rc.getChildsArrayList("0.9.6.16")) {
                    if (accumsTM.containsKey(ac.getValueAsInteger("16.0"))) {
                        Accum acc = accumsTM.get(ac.getValueAsInteger("16.0"));
                        accumsTM.put(ac.getValueAsInteger("16.0"),
                            new Accum(ac.getValueAsInteger("16.0"),
                                acc.accumB < ac.getValueAsInteger("16.1") ? ac.getValueAsInteger("16.1") : acc.accumB,
                                acc.accumA > ac.getValueAsInteger("16.3") ? ac.getValueAsInteger("16.3") : acc.accumA,
                                acc.accumC + ac.getValueAsInteger("16.2")));
                    }
                    else
                        accumsTM.put(ac.getValueAsInteger("16.0"),
                            new Accum(ac.getValueAsInteger("16.0"), ac.getValueAsInteger("16.1"), ac.getValueAsInteger("16.3"), ac.getValueAsInteger("16.2")));
                }
                accums.addAll(accumsTM.entrySet());
                for (BERFlatRecord dc: rc.getChildsArrayList("0.9.7.16")) {
                    if(dedaccsTM.containsKey(dc.getValueAsInteger("16.0"))) {
                        DedicatedAcc dea = dedaccsTM.get(dc.getValueAsInteger("16.0"));
                        dedaccsTM.put(dc.getValueAsInteger("16.0"), 
                                new DedicatedAcc(
                                        dc.getValueAsInteger("16.0"), 
                                        dea.daB < dc.getValueAsInteger("16.1.0")/Math.pow(10, dc.getValueAsInteger("16.1.1")) ? dc.getValueAsInteger("16.1.0")/Math.pow(10, dc.getValueAsInteger("16.1.1")) : dea.daB,
                                        dea.daA > dc.getValueAsInteger("16.2.0")/Math.pow(10, dc.getValueAsInteger("16.2.1")) ? dc.getValueAsInteger("16.2.0")/Math.pow(10, dc.getValueAsInteger("16.2.1")) : dea.daA,
                                        dea.daC += dc.getValueAsInteger("16.3.0")/Math.pow(10, dc.getValueAsInteger("16.3.1")),
                                        dea.daUB < dc.getValueAsInteger("16.11") ? dc.getValueAsInteger("16.11") : dea.daUB,
                                        dea.daUA < dc.getValueAsInteger("16.12") ? dc.getValueAsInteger("16.12") : dea.daUA,
                                        dea.daUC += dc.getValueAsInteger("16.13"),
                                        dc.getValueAsInteger("16.4"), 
                                        dc.getValueAsInteger("16.14"),
                                        dc.getValueAsString("16.5") != null && dea.expDateB.compareTo(dc.getValueAsString("16.5")) > 0 ? dc.getValueAsString("16.5") : dea.expDateB,
                                        dc.getValueAsString("16.6") != null && dea.expDateA.compareTo(dc.getValueAsString("16.6")) < 0 ? dc.getValueAsString("16.6") : dea.expDateA ) );
                    }
                    else 
                        dedaccsTM.put(dc.getValueAsInteger("16.0"), 
                                new DedicatedAcc(
                                        dc.getValueAsInteger("16.0"), 
                                        dc.getValueAsInteger("16.1.0")/Math.pow(10, dc.getValueAsInteger("16.1.1")),
                                        dc.getValueAsInteger("16.2.0")/Math.pow(10, dc.getValueAsInteger("16.2.1")),
                                        dc.getValueAsInteger("16.3.0")/Math.pow(10, dc.getValueAsInteger("16.3.1")),
                                        dc.getValueAsInteger("16.11"),
                                        dc.getValueAsInteger("16.12"),
                                        dc.getValueAsInteger("16.13"),
                                        dc.getValueAsInteger("16.4"), 
                                        dc.getValueAsInteger("16.14"),
                                        dc.getValueAsString("16.5"),
                                        dc.getValueAsString("16.6")) );
                }

                charge += rc.getValueAsInteger("0.9.13.0")/Math.pow(10, rc.getValueAsInteger("0.9.13.1"));
            }
            dedaccs.addAll(dedaccsTM.entrySet());

            StringBuilder sids_str = new StringBuilder();
            for (Object val: sids.toArray())
                sids_str.append((Long)val).append(",");
            System.out.println("ccServiceIdentifier => "+ sids_str.toString().substring(0, sids_str.length()-1 < 0 ? 0 : sids_str.length()-1) );
            System.out.println("ccTimeUnit => " + timeunit);
            System.out.println("ccTotalOctetsUnit => " + octetunit);
            System.out.println("ccServiceSpecificUnit => " + servspecific);
            System.out.println("ccRoamingPosition => " + berRecord.getValueAsInteger("6.13.0.7"));
            System.out.println("ccServiceClassID => " + berRecord.getValueAsInteger("6.13.0.9.1"));
            System.out.println("ccAccountValueBefore => " + accBefore);
            System.out.println("ccAccountValueAfter => " + accAfter);
            int i=0;
            Iterator<Map.Entry<Long, Accum>> acci = accums.iterator();
            while (acci.hasNext() && i < 3 ) {
                Accum acc = acci.next().getValue();
                System.out.println("ccAccumulator"+(i+1)+"ID => " + acc.accumId);
                System.out.println("ccAccumulator"+(i+1)+"Before => " + acc.accumB);
                System.out.println("ccAccumulator"+(i+1)+"Change => " + acc.accumC);
                System.out.println("ccAccumulator"+(i+1)+"After => " + acc.accumA);
                i++;
            }
            i=0;
            Iterator<Map.Entry<Long, DedicatedAcc>> deai = dedaccs.iterator();
            while (deai.hasNext() && i < 3 ) {
                DedicatedAcc dea = deai.next().getValue();
                System.out.println("ccDedicatedAccount"+(i+1)+"ID => " + dea.daID);
                System.out.println("ccDedicatedAccount"+(i+1)+"ValueBefore => " + dea.daB);
                System.out.println("ccDedicatedAccount"+(i+1)+"ValueAfter => " + dea.daA);
                System.out.println("ccDedicatedAccount"+(i+1)+"Change => " + dea.daC);
                System.out.println("ccDedicatedAccount"+(i+1)+"CampaignID => " + dea.cmgnID);
                System.out.println("ccAccount"+(i+1)+"ExpiryDateBefore => " + dea.expDateB);
                System.out.println("ccAccount"+(i+1)+"ExpiryDateAfter => " + dea.expDateA);
                System.out.println("ccDA"+(i+1)+"UnitsBefore => " + dea.daUB);
                System.out.println("ccDA"+(i+1)+"UnitsAfter => " + dea.daUA);
                System.out.println("ccDA"+(i+1)+"UnitsChange => " + dea.daUC);
                System.out.println("ccDA"+(i+1)+"UnitType => " + dea.daUT);
                i++;
            }
            
            System.out.println("ccFamilyAndFriendsID => " + berRecord.getValueAsInteger("6.13.0.9.8"));
            System.out.println("ccFamilyAndFriendsNo => " + formatNumber(berRecord.getValueAsOctet("6.13.0.9.9")));
            System.out.println("ccServiceOfferings => " + berRecord.getValueAsInteger("6.13.0.9.10"));
            System.out.println("ccAccountValueDeducted => " + charge);
            System.out.println("apn => " + getCCParamsString(16778235, berRecord.getChildsArrayList("6.13.0.10.16")));
            System.out.println("chargingId => " + getCCParamsInteger(16778226, berRecord.getChildsArrayList("6.13.0.10.16")));
            sids_str = new StringBuilder();
            for (Object val: rids.toArray())
                sids_str.append((Long)val).append(",");
            System.out.println("ratingGroup => "+ sids_str.toString().substring(0, sids_str.length()-1 < 0 ? 0 : sids_str.length()-1) );
            System.out.println("cellid => " +getCCParamsOctet(16778250, berRecord.getChildsArrayList("6.13.0.10.16")) +
                    (getCCParamsOctet(16778251, berRecord.getChildsArrayList("6.13.0.10.16")) != null ? getCCParamsOctet(16778251, berRecord.getChildsArrayList("6.13.0.10.16")) : getCCParamsOctet(16778252, berRecord.getChildsArrayList("6.13.0.10.16"))) );
            System.out.println("locno => " + getCCParamsString(16778217, berRecord.getChildsArrayList("6.13.0.10.16")));
            System.out.println("opnumber  => " + getCCParamsPartyNum(16778219, berRecord.getChildsArrayList("6.13.0.10.16")));

            System.out.println("exttext => " + getCCParamsString(16777417, berRecord.getChildsArrayList("6.13.0.10.16")));

            System.out.println("mnpPrefix => " + getCCParamsString(16778220, berRecord.getChildsArrayList("6.13.0.10.16")));
            System.out.println("mnpResult => " + getCCParamsInteger(16778222, berRecord.getChildsArrayList("6.13.0.10.16")));


            //
            // System.out.println("ccServiceIdentifier => "+berRecord.getValueAsInteger("6.13.0.1"));

            //System.out.println(" match: "+ new BERFlatQuery(berRecord).filter("^6\\.10\\.16\\[([0-9]+)\\].*").getMatchPart());

            //berRecord.getChildsRegexp("^6\\.10\\.16.*").printRecord();

            //for (BERFlatRecord rc: berRecord.getChildsArrayList("6.13.0") )
            //        rc.printRecord();

            //System.out.println("string val: 6.10.16[0].0 => "+berRecord.getValueAsInteger("6.10.16[0].0"));
            //System.out.println("string val: 6.10.16[1].0 => "+berRecord.getValueAsInteger("6.10.16[1].0"));
            /// System.out.println(berDecoder.getCurrentFilePos());
                    
            /*    
            System.out.println("string val: 12.16[0].4[0].1 => "+berRecord.getValueAsOctet("12.16[0].4[0].1"));
            System.out.println("string val: 2.15 => "+berRecord.getValueAsOctet("2.15"));
            System.out.println("string val: 2.15 => "+berRecord.getValueAsInteger("2.15"));
            System.out.println("string val: 12.11 => "+berRecord.getValueAsInteger("12.11"));
            System.out.println("string val: 12.6 => "+berRecord.getValueAsInteger("12.6"));
            System.out.println("string val: 12.11 => "+berRecord.getValueAsInteger("12.11"));
            */
            System.out.println();
        }
    }


}
