/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.azercell.bigdata.ber;

import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author itrahim
 */
public class BERFieldComposite extends BERField {
    
    protected final boolean isComposite = true;
    
    //protected HashMap<String, BERField> childFields;
    protected ArrayList<BERField> childFields;
}
