package com.azercell.bigdata.ber;


import java.util.ArrayList;
import java.util.TreeMap;
import java.util.regex.Pattern;

/**
 * Created by rahimahmedov on 9/7/14.
 */
public class BERFlatRecord {

    private final TreeMap<String, short[]> dataStore;
    private final ArrayList<Byte> rawdata;

    BERFlatRecord(){
        this.dataStore = new TreeMap<String, short[]>();
        this.rawdata = new ArrayList<Byte>(8192);
    }

    public void addField(String tagAddr, short[] value) {
        this.dataStore.put(tagAddr, value);
        for (short aValue : value) rawdata.add((byte) aValue);
    }

    public void addByte(byte rowbyte) {
        this.rawdata.add(rowbyte);
    }
    
       
    public void printRecord () {

        for (String key: this.dataStore.keySet()) {
            System.out.print(key + ": ");
            short [] val = this.dataStore.get(key);
            for (short aVal : val) System.out.printf("%02x", aVal);
            System.out.println();
        }
    }
    
    public void printRawData(int width) {
        for (int i=0; i<this.rawdata.size(); i++){
            System.out.printf("%02x ", this.rawdata.get(i));
            if ( (i+1) % width == 0)
                System.out.print("\n");                    
        }
        System.out.println();
    }


    public String getValueAsString(String key) {
        if (this.dataStore.containsKey(key)) {
            short[] val = this.dataStore.get(key);
            StringBuilder sb = new StringBuilder();
            for (short aVal : val) sb.append(new Character((char) aVal));
            return sb.toString();
        }
        else return null;
    }

    public String getValueAsOctet(String key) {
        if (this.dataStore.containsKey(key)) {
            short[] val = this.dataStore.get(key);
            StringBuilder sb = new StringBuilder();
            for (short aVal : val) sb.append(String.format("%02x", aVal));
            return sb.toString();
        }
        else return null;
    }

    public long getValueAsInteger(String key) {
        if (this.dataStore.containsKey(key)) {
            short[] val = this.dataStore.get(key);
            long retVal = val[0];
            for (int i = 1; i < val.length; i++)
                retVal = (retVal << 8) + val[i];
            return retVal;
        }
        else return 0;
    }

    public ArrayList<Byte> getRawData() {
        return this.rawdata;
    }

    public byte[] getByteArray() {
        byte[] rb = new byte[this.rawdata.size()];
        for (int i=0; i<rb.length; i++)
            rb[i] = this.rawdata.get(i);
        return rb;
    }

    public TreeMap<String, short[]> getDataStore() {
        return this.dataStore;
    }

    public BERFlatRecord getChildsRegexp(String regexp) {
        ArrayList<String>  tagIDs = new ArrayList<String>(this.dataStore.keySet());
        BERFlatRecord res = new BERFlatRecord();
        Pattern p = Pattern.compile(regexp);
        for(String tag: tagIDs) {
            if (p.matcher(tag).matches()) {
                res.addField(tag, this.dataStore.get(tag));
            }
        }
        return res;
    }

    //tupoy mentiqle
    public ArrayList<BERFlatRecord> getChildsArrayList(String tagId) {

        String prefTag = tagId.split("\\.")[tagId.split("\\.").length-1];

        ArrayList<BERFlatRecord> res = new ArrayList<BERFlatRecord>();
        ArrayList<String>  tagIDs = new ArrayList<String>(this.dataStore.keySet());
        BERFlatRecord rec = new BERFlatRecord();
        for(String tag: tagIDs) {
            if (tag.startsWith(tagId + ".")){

                rec.addField(prefTag + "." + tag.substring(tagId.length()+1, tag.length()), this.dataStore.get(tag));
            }
        }
        if (!rec.dataStore.isEmpty())
            res.add(rec);

        rec = new BERFlatRecord();
        int maxidx = 0;
        for (String tag: tagIDs) {
            if (tag.startsWith(tagId+"[")) {
                int idx = Integer.valueOf( tag.substring(tagId.length()+1, tag.indexOf("]", tagId.length())) );
                if (idx != maxidx) {
                    maxidx = idx;
                    if (!rec.dataStore.isEmpty())
                        res.add(rec);
                    rec  = new BERFlatRecord();
                }
                rec.addField( prefTag + "." + tag.substring(tag.indexOf("]", tagId.length())+2, tag.length()), this.dataStore.get(tag) );
            }
        }

        if (!rec.dataStore.isEmpty())
            res.add(rec);

        return res;
    }


}
